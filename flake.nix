{
  description = "Lazy's packages";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable";

    systems.url = "github:nix-systems/default";

    flake-utils = {
      url = "github:numtide/flake-utils";
      inputs.systems.follows = "systems";
    };

    uwal = {
      url = "gitlab:sidmoreoss/uwal/v2.0.0";
      flake = false;
    };

    scripts = {
      url = "gitlab:lazylinux/programs/main";
      flake = false;
    };

    klassy = {
      url = "github:paulmcauley/klassy";
      flake = false;
    };

    plasma-applet-eventcalendar = {
      # url = "github:Zren/plasma-applet-eventcalendar/master";
      url = "github:ALikesToCode/plasma-applet-eventcalendar/plasma-6";
      flake = false;
    };

    plasma-applet-split-clock = {
      url = "github:bainonline/split-clock";
      flake = false;
    };

    plasma-applet-thermalmonitor = {
      # url = "invent.kde.org:siddharthmore/thermalmonitor";
      url = "gitlab:siddharthmore/thermalmonitor?host=invent.kde.org";
      flake = false;
    };

    catppuccin-konsole = {
      url = "github:catppuccin/konsole";
      flake = false;
    };
  };

  outputs =
    { flake-utils, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system: {
      packages = import ./packages { inherit inputs system; };

      devShells = import ./shells { inherit inputs system; };
    });
}
