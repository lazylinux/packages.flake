# Lazy's Packages

Flake for Lazy's packages

## Description

This Nix expression defines Lazy's package configuration. It pulls in various packages from different sources and provides a user-friendly version number based on the last modified date.

## Inputs

- **nixpkgs**: Source URL for Nixpkgs, currently set to `github:nixos/nixpkgs/nixos-unstable`.
- **uwal**: Source URL for the uwal package.
- **scripts**: Source URL for additional scripts.
- **plasma-applet-eventcalendar**: Source URL for the Plasma Applet EventCalendar package.
- **catppuccin-konsole**: Source URL for the catppuccin Konsole package.

## Outputs

- **packages**: A set of packages instantiated for supported system types (`x86_64-linux` in this case).

## Flake Support

This configuration also supports using flakes. The supported packages via flakes are:

- **nixpkgs**: The Nixpkgs flake from `github:nixos/nixpkgs/nixos-unstable`.
- **uwal**: The uwal package from `gitlab:sidmoreoss/uwal/master`.
- **scripts**: Additional scripts from `gitlab:lazylinux/programs/main`.
- **plasma-applet-eventcalendar**: The Plasma Applet EventCalendar package from `github:ALikesToCode/plasma-applet-eventcalendar/plasma-6`.
- **catppuccin-konsole**: The catppuccin Konsole package from `github:catppuccin/konsole`.

## Usage

### Using `nix run`

You can use `nix run` to execute commands provided by the packages. For example:

```bash
nix run .#packages.x86_64-linux.plasma-applet-eventcalendar
```

This will run the Plasma Applet EventCalendar package.

To use a package from a flake, you can specify the flake reference with `nix run`. For example:

```bash
nix run gitlab:lazylinux/packages.flake#uwal
```

This will run the uwal package from the `gitlab:lazylinux/packages` flake.

### Using Direct Instantiation

To use these packages directly, you can instantiate them with your preferred system type:

```nix
let
  lazyPackages = import ./path/to/lazys-packages.nix;
in
lazyPackages.packages."x86_64-linux"
```

## Additional Notes

- The version number is generated based on the last modified date of this configuration.
- Supported system types: `x86_64-linux`.
