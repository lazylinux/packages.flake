{
  pkgs,
  src,
  ...
}:
pkgs.stdenv.mkDerivation {
  pname = "scripts";

  version = "v1.0";

  src = "${src}/programs";

  nativeBuildInputs = [
    pkgs.autoPatchelfHook
  ];

  sourceRoot = "./programs";

  installPhase = ''
    for program in *; do
      install -m755 -D $program $out/bin/$program
    done
  '';
}
