{
  pkgs,
  src,
  ...
}:
pkgs.stdenv.mkDerivation {
  pname = "catppuccin-konsole";

  version = "1.0.0";

  inherit src;

  phases = ["unpackPhase" "installPhase"];

  installPhase = ''
    mkdir -p $out/share/konsole
    cp -r $src/themes/*.colorscheme $out/share/konsole
  '';
}
