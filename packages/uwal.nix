{
  pkgs,
  lib,
  src,
  ...
}:
pkgs.stdenv.mkDerivation {
  pname = "uwal";

  version = "v2.0";

  inherit src;

  nativeBuildInputs = with pkgs; [
    autoPatchelfHook
    makeWrapper
  ];

  buildInputs = with pkgs; [
    wget
  ];

  patchPhase = ''
    substituteInPlace install.sh \
    --replace "\"wallpaper_urls.txt\"" "\"$src/wallpaper_urls.txt\"" \
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp install.sh $out/bin/uwal
    wrapProgram $out/bin/uwal \
    --prefix PATH : ${
      lib.makeBinPath [
        pkgs.aria2
        pkgs.coreutils
        pkgs.gnutar
        pkgs.rsync
        pkgs.pv
        pkgs.gdown
      ]
    }
  '';
}
