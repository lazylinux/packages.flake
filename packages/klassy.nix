{
  pkgs,
  lib,
  src,
}:
let
  qtMajorVersion = lib.versions.major pkgs.kdePackages.qtbase.version;
in
pkgs.stdenv.mkDerivation {
  pname = "klassy-qt${qtMajorVersion}";
  version = "plasma6.2";

  inherit src;

  nativeBuildInputs = with pkgs; [
    cmake
    ninja
    kdePackages.extra-cmake-modules
    kdePackages.wrapQtAppsHook
  ];

  buildInputs =
    with pkgs.kdePackages;
    [
      qtbase
      qtdeclarative
      qttools
      frameworkintegration
      kcmutils
      kdecoration
      kiconthemes
      kwindowsystem
    ]
    ++ lib.optionals (qtMajorVersion == "5") [
      qtx11extras
      kconfigwidgets
      kirigami2
    ]
    ++ lib.optionals (qtMajorVersion == "6") [
      qtsvg
      kcolorscheme
      kconfig
      kcoreaddons
      kdecoration
      kguiaddons
      ki18n
      kirigami
      kwidgetsaddons
    ];

  cmakeFlags = map (v: lib.cmakeBool "BUILD_QT${v}" (v == qtMajorVersion)) [
    "5"
    "6"
  ];

  passthru.updateScript = pkgs.gitUpdater { };

  meta =
    {
      description = "Highly customizable binary Window Decoration, Application Style and Global Theme plugin for recent versions of the KDE Plasma desktop";
      homepage = "https://github.com/paulmcauley/klassy";
      platforms = lib.platforms.linux;
      license = with lib.licenses; [
        bsd3
        cc0
        gpl2Only
        gpl2Plus
        gpl3Only
        gpl3Plus # KDE-Accepted-GPL
        mit
      ];
      maintainers = with lib.maintainers; [ pluiedev ];
    }
    // lib.optionalAttrs (qtMajorVersion == "6") {
      mainProgram = "klassy-settings";
    };
}
