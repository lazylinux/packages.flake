{
  pkgs,
  src,
  ...
}:
pkgs.stdenv.mkDerivation {
  pname = "plasma-applet-split-clock";

  version = "v1.0";

  src = "${src}";

  phases = ["unpackPhase" "installPhase"];

  installPhase = ''
    mkdir -p $out/share/plasma/plasmoids
    cp -r $src $out/share/plasma/plasmoids/split-clock
  '';
}
