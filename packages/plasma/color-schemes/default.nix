{pkgs, ...}:
pkgs.stdenv.mkDerivation {
  pname = "breeze-accent";
  version = "v1.0";
  src = ./.;
  phases = ["unpackPhase" "installPhase"];
  installPhase = ''
    mkdir -p $out/share/color-schemes
    cp $src/*.colors $out/share/color-schemes
  '';
}
