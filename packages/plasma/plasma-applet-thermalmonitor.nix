{
  pkgs,
  src,
  ...
}:
pkgs.stdenv.mkDerivation {
  pname = "plasma-applet-thermalmonitor";

  version = "v1.0";

  src = "${src}/package";

  phases = ["unpackPhase" "installPhase"];

  installPhase = ''
    mkdir -p $out/share/plasma/plasmoids
    cp -r $src $out/share/plasma/plasmoids/org.kde.olib.thermalmonitor
  '';
}
