{
  inputs,
  system,
  ...
}:
let
  pkgs = inputs.nixpkgs.legacyPackages.${system};

  callPackage = pkgs.callPackage;
in
{
  color-schemes = callPackage ./plasma/color-schemes { };

  catppuccin-konsole = callPackage ./catppuccin-konsole.nix { src = inputs.catppuccin-konsole; };

  plasma-applet-eventcalendar = callPackage ./plasma/plasma-applet-eventcalendar.nix {
    src = inputs.plasma-applet-eventcalendar;
  };

  plasma-applet-split-clock = callPackage ./plasma/plasma-applet-split-clock.nix {
    src = inputs.plasma-applet-split-clock;
  };

  plasma-applet-thermalmonitor = callPackage ./plasma/plasma-applet-thermalmonitor.nix {
    src = inputs.plasma-applet-thermalmonitor;
  };

  scripts = callPackage ./scripts.nix { src = inputs.scripts; };

  sddm-background = callPackage ./sddm-background.nix { };

  uwal = callPackage ./uwal.nix { src = inputs.uwal; };

  klassy = callPackage ./klassy.nix { src = inputs.klassy; };
}
