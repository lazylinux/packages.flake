{
  pkgs,
  background ? "",
  ...
}:
pkgs.stdenv.mkDerivation {
  pname = "sddm-background";
  version = "v1.0";
  src = ./.;
  phases = [
    "unpackPhase"
    "installPhase"
  ];
  installPhase = ''
    mkdir -p $out/share/sddm/themes/breeze/
    echo -e "[General]\ntype=image\nbackground=${background}" > $out/share/sddm/themes/breeze/theme.conf.user
  '';
}
