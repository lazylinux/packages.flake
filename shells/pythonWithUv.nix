{pkgs, ...}:
pkgs.mkShell {
  nativeBuildInputs = with pkgs; [
    python3
    uv
  ];

  # shellHook = ''
  #   zsh
  # '';
}
