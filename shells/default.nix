{
  inputs,
  system,
  ...
}: let
  pkgs = inputs.nixpkgs.legacyPackages.${system};
in {
  pythonWithUv = import ./pythonWithUv.nix {inherit pkgs inputs;};
}
